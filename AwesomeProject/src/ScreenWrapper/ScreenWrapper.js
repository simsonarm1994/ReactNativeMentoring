import React from 'react';
import {StyleSheet, View, Image, Text, TouchableOpacity} from 'react-native';

export default (params, WrappedComponent) => {
  return (props = {}) => (
    <View style={{ flex: 1, }}>
      <View style={styles.headerContainer}>
        <View style={styles.headerContainerInner}>
        <Image
          resizeMode="contain"
          style={styles.logoImage}
          source={require('../images/logo.png')}/>
        <View style={styles.container}>
          <TouchableOpacity onPress={() => props.nav.navigate('Menu')}>
            <Image
              style={styles.profileImage}
              source={require('../images/profile_icon.png')}/>
          </TouchableOpacity>
        </View>
          </View>
      </View>
      <View style={styles.screenContainer}>
      <WrappedComponent {...props}/>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: '#000',
    position: 'relative',
    flex: 0.1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  headerContainerInner:{
    flex: 0.9,
    flexDirection: 'row',
    height: 35,
    justifyContent: 'center',
  },
  logoImage:{
    height: 23,
    marginTop: 8,
  },
  container:{
    position: 'absolute',
    bottom: 0,
    right: 0,
    top: 0,
  },
  profileImage:{
    height: 30,
    width: 30,
  },
  screenContainer: {
    flex: 0.9,
  }
});
