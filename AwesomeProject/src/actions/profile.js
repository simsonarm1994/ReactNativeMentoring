import { createAction } from 'redux-actions';
import { ActionTypes} from '../constants';
import api from '../api/api';


/*
 |--------------------------------------------------------------------------
 | Get Profile Data
 |--------------------------------------------------------------------------
 */

const getProfileRequest = createAction(ActionTypes.profile.get.request);
const getProfileResponse = createAction(ActionTypes.profile.get.response);
const getProfileError = createAction(ActionTypes.profile.get.error);

export function getProfile() {
  return (dispatch) => {
    dispatch(getProfileRequest());
    return api.getProfile()
      .then(data => {
        return setTimeout( () => dispatch(getProfileResponse(data.data)), 2000) })
      .catch(error => dispatch(getProfileError(error)));
  };
}

