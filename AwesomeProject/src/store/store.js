import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import {
  profileReducer
} from '../reducers';

const reducers = combineReducers({
  profileReducer
});

const store = createStore(
  reducers,
  compose(
    applyMiddleware(thunk)
  )
);

export default store;
