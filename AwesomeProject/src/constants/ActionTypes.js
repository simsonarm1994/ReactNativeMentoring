export default {
  profile: {
    get: {
      request: 'PROFILE_GET_REQUEST',
      response: 'PROFILE_GET_RESPONSE',
      error: 'PROFILE_GET_ERROR'
    }
  },
};
