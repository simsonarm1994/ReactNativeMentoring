import { handleActions } from 'redux-actions';
import { ActionTypes } from '../constants';

const defaultState = {
  isFetch: false,
  data: null,
  error: null
};


const profileReducer = handleActions({
  [ActionTypes.profile.get.request]: (state, { payload }) => ({
    ...state,
    isFetch: true
  }),
  [ActionTypes.profile.get.error]: (state, { payload }) => ({ ...state, isFetch: false, error: payload }),
  [ActionTypes.profile.get.response]: (state, { payload }) => ({
    ...state,
    isFetch: false,
    data: payload
  })
}, defaultState);

export default profileReducer;
