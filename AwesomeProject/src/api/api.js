import axios from 'axios';
// 'https://facebook.github.io/react-native/movies.json'
const api = axios.create({
  baseURL: 'http://10.22.21.173:8080/',
  // headers: {
  //   Accept: 'application/json',
  //   'Content-Type': 'application/json',
  // },
});

const getProfile = () => api.get('/profile.json');

export default {
  getProfile
}