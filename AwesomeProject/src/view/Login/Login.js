import React from 'react';
import {StyleSheet, View, Image, Text, KeyboardAvoidingView} from 'react-native';
import Form from './Form';

class Login extends React.Component {
  render() {
    return (
      <KeyboardAvoidingView
        behavior='padding'
        style={styles.loginContainer}>
        <View style={styles.loginWrap}>
          <View style={styles.loginImgContainer}>
            <Image
              style={styles.logoImage}
              resizeMode="contain"
              source={require('../../images/title.png')}/>
          </View>
          <Form/>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  loginContainer: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  loginWrap: {
    flex: 0.9,
    alignItems: 'stretch',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  loginImgContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-end',
  },
  logoImage: {
    flex: 1,
    width: null,
    height: 100,
    marginBottom: 55
  }
});


export default Login
