import React from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity, Text} from 'react-native';

const COLOR_GREEN = '#A3C644';
const COLOR_WHITE = '#fff';
const COLOR_GREY = '#A2A2A2';

 class Form extends React.Component {
  render() {
    return (
     <View style={styles.formContainer}>
        <TextInput
          placeholder='Username'
          autoCorrect={false}
          style={styles.input}
          returnKeyType='next'
          onSubmitEditing={() => this.passwordInput.focus()}
          underlineColorAndroid={COLOR_WHITE}/>
        <TextInput
          placeholder='Password'
          style={styles.input}
          returnKeyType='go'
          secureTextEntry
          ref={(input) => this.passwordInput = input}
          underlineColorAndroid={COLOR_WHITE}
        />
        <TouchableOpacity style={styles.buttonContainer}>
          <Text style={styles.buttonText}>LOGIN</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  formContainer: {
  },
  input: {
    height: 50,
    paddingHorizontal: 15,
    marginBottom: 30,
    color: COLOR_GREY,
    backgroundColor: COLOR_WHITE,
    borderColor: COLOR_GREY,
    borderWidth: 1,
    fontSize: 18,
  },
  buttonContainer: {
    backgroundColor: COLOR_GREEN,
    paddingVertical: 15,
    marginBottom: 15
  },
  buttonText: {
    color: COLOR_WHITE,
    textAlign: 'center',
    fontSize: 21,
    fontWeight: '500'
  }
});


export default Form