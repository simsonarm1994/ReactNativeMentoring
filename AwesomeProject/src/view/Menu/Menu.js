import React from 'react';
import {connect} from 'react-redux';
import { StyleSheet, View, Image, Text, TouchableOpacity} from 'react-native';
// import {profileAction} from '../../actions';

class Menu extends React.Component {

  render() {
    return (
      <View style={styles.menuContainer}>
        <View style={styles.headerContainer}>
          <View style={styles.headerContainerInner}>
        <TouchableOpacity onPress={() => this.props.nav.navigateBack()}>
          <Image
              resizeMode="contain"
              style={styles.logoImage}
              source={require('../../images/close.png')}/>
        </TouchableOpacity>
          </View>
        </View>
        <View style={styles.titleContainer}>
          <Text>Username</Text>
        </View>
        <View style={styles.contentContainer}>
          <Text>content</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  menuContainer: {
    flex: 1,
    backgroundColor:'rgba(0,0,0,0.8)',
    flexDirection: 'column',
    alignItems: 'stretch',
  },
  headerContainer:{
    flex: 0.1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  headerContainerInner:{
    flex: 0.9,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  logoImage: {
    height: 35,
    width: 35,
  },
  titleContainer:{
    flex: 0.1,
    flexDirection: 'row',
    // backgroundColor: 'blue'
  },
  contentContainer: {
    flex: 0.8,
    flexDirection: 'row',
    // backgroundColor: 'white'
  }
});

export default Menu;
//
// export default connect(
//   state => ({
//     profileReducer: state.profileReducer
//   }),
//   {
//     ...profileAction
//   }
// )(Profile);

