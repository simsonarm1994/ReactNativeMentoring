import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet, View, Image, Text, TouchableOpacity, StatusBar} from 'react-native';
import {profileAction} from '../../actions';

class Profile extends React.Component {
  componentDidMount() {
    this.props.getProfile();
  }

  render() {
    console.log(this.props);
    let success = this.props.profileReducer.data && !this.props.profileReducer.error;
    return (
      <View style={styles.profileContainer}>
        <View style={styles.profileContainerWrap}>
          <View style={styles.profileContainerInner}>
            <View style={styles.imgContainer}>
              <Image
                style={styles.image}
                resizeMode="contain"
                source={require('../../images/developer.png')}/>
            </View>
            {this.props.profileReducer.isFetch && <Text>load...</Text>}
            {success && <Text>{this.props.profileReducer.data.personal.percent}</Text>}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  profileContainer: {
    flex: 1,
    backgroundColor: 'red',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  profileContainerWrap: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'blue',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  profileContainerInner: {
    flex: 0.9,
    flexDirection: 'column',
    backgroundColor: 'red',
    alignItems: 'stretch',
  },
  imgContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  image: {
    flex: 1,
    width: null,
    height: 300,
  }
});

export default connect(
  state => ({
    profileReducer: state.profileReducer
  }),
  {
    ...profileAction
  }
)(Profile);

