import React from 'react';
import {AppRegistry, StyleSheet, View, StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import {
  createRouter,
  NavigationProvider,
  StackNavigation,
} from '@expo/ex-navigation';
import store from './src/store/store';

import {Login, Profile, Menu} from './src/view';

import ScreenWrapper from './src/ScreenWrapper/ScreenWrapper';

import SwipeNavigator from 'react-native-swipe-navigation';
//https://github.com/AZZB/react-native-swipe-navigation


const Navigator = SwipeNavigator({
  Login: {
    screen: Login,
    right: 'Profile',
  },

  Profile: {
    screen: ScreenWrapper({}, Profile),
    type: 'push'
  },

  Menu: {
    screen: Menu,
    type: 'over'
  },
});

const App = () => (
      <Provider store={store}>
        <View style={rootsStyles.root}>
          <StatusBar
            backgroundColor="blue"
            barStyle="light-content"
          />
          <Navigator/>
        </View>
      </Provider>
);

const rootsStyles = StyleSheet.create({
  root: {
    flex: 1,
  }
});

export default App;

